//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	/* {"⌨", "sb-kbselect", 0, 30}, */
	/* {"", "cat /tmp/recordingicon 2>/dev/null",	0,	9}, */
	/* {"",	"sb-tasks",	10,	26}, */
	{"",	"~/.local/bin/statusbar/sb-music",	1,	11},
	{"",	"~/.local/bin/statusbar/sb-weather",	18000,	5},
	/* {"⟁",	"INSTANCE=bat ~/.local/bin/statusbar/sb-crypto",	9000,	20}, */
	/* {"ɱ",	"INSTANCE=xmr ~/.local/bin/statusbar/sb-crypto",	9000,	24}, */
	{"🖴",	"~/.local/bin/statusbar/sb-disk",	20,	9},
	{"🧠",	"~/.local/bin/statusbar/sb-memory",	10,	14},
	{"",	"~/.local/bin/statusbar/sb-battery",	10,	14},
	{"",	"~/.local/bin/statusbar/sb-news",	0,	6},
	{"",	"~/.local/bin/statusbar/sb-mail",	180,	12},
	{"",	"~/.local/bin/statusbar/sb-internet",	5,	4},
	{"",	"~/.local/bin/statusbar/sb-volume",	0,	10},
	{"",	"~/.local/bin/statusbar/sb-time",	1,	1},
};

//Sets delimiter between status commands. NULL character ('\0') means no delimiter.
static char *delim = " | ";

// autocmd BufWritePost ~/.local/src/dwmblocks/config.h !cd ~/.local/src/dwmblocks/; sudo make install && { killall -q dwmblocks;setsid dwmblocks & }
